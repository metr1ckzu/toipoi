from rest_framework import serializers
#from dancing_group.models import
from restaurant.models import Kitchen, Room, Restaurant
#from toastmaster.models import
#from user.models import


class KitchenSerializer(serializers.ModelSerializer):

    class Meta:
        model = Kitchen
        fields = ('id', 'name')


class RoomSerializer(serializers.ModelSerializer):

    class Meta:
        model = Room
        fields = ('id', 'name', 'capacity')


class RestaurantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'kitchen', 'room', 'bill', 'time_work', 'telephone_number', 'address')

