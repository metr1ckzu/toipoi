from rest_framework import viewsets


#from dancing_group.models import
from restaurant.models import Kitchen, Room, Restaurant
#from toastmaster.models import
#from user.models import
from .serializers import KitchenSerializer, RoomSerializer, RestaurantSerializer


class KitchenViewSet(viewsets.ModelViewSet):
    queryset = Kitchen.objects.all()
    serializer_class = KitchenSerializer


class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer


class RestaurantViewSet(viewsets.ModelViewSet):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer