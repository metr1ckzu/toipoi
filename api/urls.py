from rest_framework import routers
from .views import KitchenViewSet, RoomViewSet, RestaurantViewSet


router = routers.DefaultRouter()
router.register(r'kitchens', KitchenViewSet, base_name='kitchens')
router.register(r'rooms', RoomViewSet, base_name='rooms')
router.register(r'restaurants', RestaurantViewSet, base_name='restaurants')

urlpatterns = router.urls