from django.apps import AppConfig


class DancingGroupConfig(AppConfig):
    name = 'dancing_group'
