from django.db import models


class Kitchen(models.Model):
    name = models.CharField(max_length=255, verbose_name='Кухня')

    class Meta:
        verbose_name_plural = 'Кухни'
        verbose_name = 'Кухня'

    def __str__(self):
        return self.name


class Room(models.Model):
    name = models.CharField(max_length=255, verbose_name='Зал')
    capacity = models.IntegerField(verbose_name='Количество людей')

    class Meta:
        verbose_name_plural = 'Залы'
        verbose_name = 'Зал'

    def __str__(self):
        return self.name


class Restaurant(models.Model):
    name = models.CharField(max_length=255, verbose_name='Ресторан')
    kitchen = models.ForeignKey(Kitchen, verbose_name='Кухня')
    room = models.ForeignKey(Room, verbose_name='Количество людей')
    bill = models.DecimalField(decimal_places=0, max_digits=50, verbose_name='Банкетный счет')
    time_work = models.CharField(max_length=255, verbose_name='Время работы')
    telephone_number = models.CharField(max_length=255, verbose_name='Телефон')
    address = models.CharField(max_length=255, verbose_name='Адрес')

    class Meta:
        verbose_name_plural = 'Рестораны'
        verbose_name = 'Ресторан'

    def __str__(self):
        return self.name

