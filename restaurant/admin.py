from django.contrib import admin
from .models import Kitchen, Room, Restaurant


class KitchenAdminModel(admin.ModelAdmin):
    list_display = ['name']
    list_display_links = ['name']
    list_filter = ['name']
    search_fields = ['name']

    class Meta:
        model = Kitchen


class RoomAdminModel(admin.ModelAdmin):
    list_display = ['name', 'capacity']
    list_display_links = ['name', 'capacity']
    list_filter = ['name', 'capacity']
    search_fields = ['name', 'capacity']

    class Meta:
        model = Room


class RestaurantAdminModel(admin.ModelAdmin):
    list_display = ['name', 'kitchen', 'room', 'bill', 'time_work', 'telephone_number', 'address']
    list_display_links = ['name', 'kitchen', 'room', 'bill', 'time_work', 'telephone_number', 'address']
    list_filter = ['name', 'kitchen', 'room', 'bill', 'time_work', 'telephone_number', 'address']
    search_fields = ['name', 'kitchen', 'room', 'bill', 'time_work', 'telephone_number', 'address']

    class Meta:
        model = Kitchen

admin.site.register(Kitchen, KitchenAdminModel)
admin.site.register(Room, RoomAdminModel)
admin.site.register(Restaurant, RestaurantAdminModel)